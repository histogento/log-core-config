**Histogento Log Core Config Module**

Ce module fait partie des modules Histogento développés par Alexis OLIVE.

**Installation**

Pour installer ce module, vous pouvez utiliser Packagist. Exécutez les commandes suivantes :

```
composer require histogento/log-core-config
bin/magento setup:upgrade
bin/magento setup:di:compile
```

**Rôle du module**

Ce module enregistre toutes les modifications apportées à la table de configuration principale (core config data) de Magento. Toutes les modifications, y compris les anciennes et nouvelles valeurs, ainsi que le nom de l'administrateur responsable des modifications, sont consignées dans un fichier spécifique : var/log/logCoreConfig.log.

N'hésitez pas à contribuer en signalant les problèmes ou en proposant des améliorations.
